# CI Templates

This repository stores all common CI jobs which could be included in projects pipelines.
It includes jobs for:
* Docker build
* Sonar analysis
* Python or JS lint, tests, publication etc..
* Auto-release

## Getting started

To use some of these templates in your project just include them in your `.gitlab-ci.yml`:

```yaml
include:
  - project: 'foundation/ci-templates'
    ref: master
    file: '/templates/<template_name>.gitlab-ci.yml'
```

Each template needs some prerequisite variables. Take care of setting them up in your project. Most of them are already setup @Foundation level.

### Ordering stages

Default order of stages is setup in each template but should be overriden in your `.gitlab-ci.yml` to ensure your pipeline will be well executed.

```yaml
# Stages example
stages:
 - validation
 - test
 - build
 - publish
 - deploy-development
 - deploy-staging
 - deploy-production
```
